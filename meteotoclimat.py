import csv
import urllib.request
import io
import re

def print_stat (libelle, row_i):
    if row_i > 0:
        for i in range (1, 14):
            value = data[row_i][i]
            if value == '.':
                value = '0'
            if value == '-':
                value =''
            print (f'|{libelle}-{mois_libelles[i]}={value}<br>', file=output)

def print_stat_record (libelle, row_i):
    if row_i > 0:
        for i in range (1, 14):
            print (f'|{libelle}-{mois_libelles[i]}={data[row_i][i]} | {libelle}-date-{mois_libelles[i]}={data[row_i+1][i].replace("-",".")}<br>', file=output)

def next_data_row_i (row_i):
    next_i = row_i+1
    while data[next_i][0] != '':
        if 'non disponible' in data[next_i][0]:
            return 0
        next_i += 1
    return next_i


def generate_from (fiche_id):
    global data
    data = []
    global mois_libelles
    mois_libelles = ['', 'jan', 'fev', 'mar', 'avr', 'mai', 'jui', 'jul', 'aou', 'sep', 'oct', 'nov', 'dec', 'ann']
    global output
    output = io.StringIO()

    fiche_url = f'https://donneespubliques.meteofrance.fr/FichesClim/FICHECLIM_{fiche_id}.data'

    ficheclim = urllib.request.urlopen(fiche_url).read()
    meteodatastr = ficheclim.decode('utf-8')
    with io.StringIO(meteodatastr) as meteodata:
        reader = csv.reader(meteodata, delimiter=';', skipinitialspace=True)
        for row in reader:
            data.append(row)

    row_i = TMax_i = TMin_i = TMoy_i = TRecMax_i = TRecMin_i = DIns_i = Brouillard_i = Orage_i = Grele_i = Neige_i = Rr1_i = Rr5_i = Rr10_i = PMoy_i = 0
    for row in data:
        if len(row) > 0:
            if ('maximale (Moyenne en' in row[0]):
                TMax_i = next_data_row_i (row_i)
            if ('minimale (Moyenne en' in row[0]):
                TMin_i = next_data_row_i (row_i)
            if ('moyenne (Moyenne en' in row[0]):
                TMoy_i = next_data_row_i (row_i)
            if ('Hauteur moyenne mensuelle' in row[0]):
                PMoy_i = next_data_row_i (row_i)
            if ('température la plus élevée' in row[0]):
                TRecMax_i = next_data_row_i (row_i)
            if ('température la plus basse' in row[0]):
                TRecMin_i = next_data_row_i (row_i)
            if ("Durée d'insolation" in row[0]):
                DIns_i = next_data_row_i (row_i)
            if ("Brouillard" in row[0]):
                Brouillard_i = row_i
            if ("Orage" in row[0]):
                Orage_i = row_i
            if ("Grêle" in row[0]):
                Grele_i = row_i
            if ("Neige" in row[0]):
                Neige_i = row_i
            if ("Rr >=  1 mm" in row[0]):
                Rr1_i = row_i
            if ("Rr >=  5 mm" in row[0]):
                Rr5_i = row_i
            if ("Rr >= 10 mm" in row[0]):
                Rr10_i = row_i

        row_i += 1

    p = re.compile(r'(.+?) Indicatif : (\d+), alt : (\d+)m, lat : (\d+)°(\d+)\'(\d+)"(\D), lon : (\d+)°(\d+)\'(\d+)"(\D)')
    gn = (p.findall(data[3][0]))[0]

    print (f'&lt;!-- Généré à partir de https://meteofrancetoclimat.toolforge.org : éviter de modifier. Regénérer à partir de cette adresse avec l\'identifiant {fiche_id}. --&gt;<br>', file=output) 
    print ('{{Climat<br>', file=output)
    print (f'|titre={data[2][0]} Station {gn[0]}Alt: {gn[2]}m {{{{Coord|{gn[3]}|{gn[4]}|{gn[5]}|{gn[6]}|{gn[7]}|{gn[8]}|{gn[9]}|{gn[10]}}}}}<br>', file=output)
    print (f'|source={{{{Lien Web|url={fiche_url.replace("data","pdf")}|titre=Fiche {fiche_id}|site=donneespubliques.meteofrance.fr|date={data[4][0]}|id=MétéoFrance {fiche_id}|libellé=MétéoFrance}}}}<br>', file=output)

    print_stat ('tmin', TMin_i)
    print_stat ('tmoy', TMoy_i)
    print_stat ('tmax', TMax_i)
    print_stat ('prec', PMoy_i)
    print_stat_record ('tmax-record', TRecMax_i)
    print_stat_record ('tmin-record', TRecMin_i)
    print_stat ('soleil', DIns_i)
    print_stat ('orage-jour', Orage_i)
    print_stat ('neige-jour', Neige_i)
    print_stat ('brouillard-jour', Brouillard_i)
    print_stat ('grêle-jour', Grele_i)
    print_stat ('pluie-jour+1mm', Rr1_i)
    print_stat ('pluie-jour+5mm', Rr5_i)
    print_stat ('pluie-jour+10mm', Rr10_i)

    print ('}}', file=output)

    return output.getvalue()
